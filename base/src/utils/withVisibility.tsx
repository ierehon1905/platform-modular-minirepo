import React, { ComponentType } from 'react';

// export type withVisibilityProps = {
//   isVisible?: boolean;
//   [prop: string]: any;
// };

export const withVisibility: (
  WrappedComponent: ComponentType<any>
) => ComponentType<any> = WrappedComponent => props =>
  props.isVisible ? <WrappedComponent {...props} /> : null;
