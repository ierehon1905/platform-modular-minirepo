import { ButtonProps } from 'components/Button';

export const colors = {
  white: '#FFFFFF',
  darkBg: '#2C353F',
  lightBg: '#E1E3E7',
  lightBorder: '#B5B9C7',
  accentBlueBg: '#186DEC',
  accentLightBlueBg: '#1890ff',
  pageBg: '#F4F5F9',
  currentBg: '#FFAC0B',
  fontDefaultDarkBg: '#E1E3E7',
  fontAccentDarkBg: '#F2F2F2',
  fontDefaultLightBg: '#3C3D46',
  errorRed: '#F54056',
  fontLight: '#B5B9C7',
};

export const textColorByTheme = (theme: ButtonProps['theme']) => {
  let color = colors.fontDefaultLightBg;
  if (theme === 'dark') {
    color = colors.fontDefaultDarkBg;
  } else if (['accent', 'current'].includes(theme as string)) {
    color = colors.fontAccentDarkBg;
  }
  return color;
};

export function lightenDarkenColor(hex, amt) {
  var col = hex.replace('#', '');
  if (col == hex) return hex;
  var num = parseInt(col, 16);
  var r = (num >> 16) + amt;
  var g = (num & 0x0000ff) + amt;
  var b = ((num >> 8) & 0x00ff) + amt;
  if (r > 255) r = 255;
  if (g > 255) g = 255;
  if (b > 255) b = 255;
  if (r < 0) r = 0;
  if (g < 0) g = 0;
  if (b < 0) b = 0;
  var newColor = g | (b << 8) | (r << 16);
  return `#${newColor.toString(16)}`;
}

export function smartColor(hex, amt) {
  var col = hex.replace('#', '');
  var num = parseInt(col, 16);
  var r = num >> 16;
  var b = (num >> 8) & 0x00ff;
  var g = num & 0x0000ff;
  var sAmt = (r + g + b) / 3 >= 128 ? amt : -amt;
  var minAmt = sAmt;
  if (r + sAmt > 255) if (r + sAmt < minAmt) minAmt = r + minAmt;
  if (g + sAmt > 255) if (g + sAmt < minAmt) minAmt = g + minAmt;
  if (b + sAmt > 255) if (b + sAmt < minAmt) minAmt = b + minAmt;
  var newColor = (g + minAmt) | ((b + minAmt) << 8) | ((r + minAmt) << 16);
  return `#${newColor.toString(16)}`;
}

export function isDark(hex) {
  var col = hex.replace('#', '');
  var num = parseInt(col, 16);
  var r = num >> 16;
  var b = (num >> 8) & 0x00ff;
  var g = num & 0x0000ff;
  return (r + g + b) / 3 < 128;
}

export function getRGBA(hex, opacity) {
  var col = hex.replace('#', '');
  var num = parseInt(col, 16);
  var r = num >> 16;
  var b = (num >> 8) & 0x00ff;
  var g = num & 0x0000ff;
  return `rgba(${r}, ${b}, ${g}, ${opacity})`;
}

export function smartGrayColor(hex) {
  var col = hex.replace('#', '');
  if (col == hex) return hex;
  var num = parseInt(col, 16);
  var r = num >> 16;
  var b = (num >> 8) & 0x00ff;
  var g = num & 0x0000ff;
  return (r + g + b) / 3 >= 128
    ? 'rgba(0, 0, 0, 0.1)'
    : 'rgba(255, 255, 255, 0.2)';
}

export const chatTheme = {
  vars: {
    'primary-color': colors.accentBlueBg,
    'secondary-color': colors.white,
    'tertiary-color': 'rgba(0, 0, 0, 0.01)',
  },
  MessgeList: {
    color: colors.fontLight,
  },
  Message: {
    css: {
      color: colors.fontLight,
    },
    Bubble: {
      css: {
        color: colors.fontDefaultLightBg,
        background: 'var(--secondary-color)',
        boxShadow: '1px 3px 2px rgba(0, 0, 0, 0.3)',
      },
    },
    own: {
      Bubble: {
        css: {
          color: colors.fontAccentDarkBg,
          background: 'var(--primary-color)',
          boxShadow: 'none',
        },
      },
    },
  },
  ChatList: {
    ChatListItem: {
      css: {
        marginBottom: 35,
        padding: 0,
        border: 'none',
      },
    },
  },
};
