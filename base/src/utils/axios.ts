import axios from 'axios';
import { history, store } from '../store';
import {
  setAccessToken,
  setRefreshToken,
  loginUserSuccess,
} from '../store/modules/user/actions';
import { getFromLocalStorage } from './localStorage';

let isRefreshing = false;
let failedQueue: any[] = [];

const processQueue = (error, token = null) => {
  console.log('Doing concurrent', failedQueue);
  failedQueue.forEach(prom => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  });

  failedQueue = [];
};

export const instance = axios.create({
  baseURL:
    process.env.NODE_ENV === 'production'
      ? '/api'
      : process.env.REACT_APP_DEV_BACKEND,
  headers: {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + getFromLocalStorage('accessToken'),
  },
});

instance.interceptors.request.use(
  response => {
    response.headers['Authorization'] =
      'Bearer ' + getFromLocalStorage('accessToken');
    return response;
  },
  error => error
);

const createAxiosResponseInterceptor = () => {
  const refreshInterceptor = instance.interceptors.response.use(
    response => response,
    error => {
      // Reject promise if usual error
      if (error.response.status !== 401) {
        return Promise.reject(error);
      }
      /*
       * When response code is 401, try to refresh the token.
       * Eject the interceptor so it doesn't loop in case
       * token refresh causes the 401 response
       */
      instance.interceptors.response.eject(refreshInterceptor);
      const originalRequest = error.config;
      if (isRefreshing) {
        console.log(
          '[TOKEN] Where refreshing appended to ',
          failedQueue.length + 1
        );

        return new Promise((resolve, reject) => {
          failedQueue.push({ resolve, reject });
        })
          .then(token => {
            originalRequest.headers['Authorization'] = 'Bearer ' + token;
            return instance(originalRequest);
          })
          .catch(err => {
            return Promise.reject(err);
          });
      }
      isRefreshing = true;

      console.log('[TOKEN] Where NOT refreshing ');
      return instance
        .post('/auth/refresh', {
          refreshToken: getFromLocalStorage('refreshToken'),
        })
        .then(response => {
          console.log('[TOKEN] ', response);
          store.dispatch(setAccessToken(response.data.accessToken));
          store.dispatch(setRefreshToken(response.data.refreshToken));
          store.dispatch(loginUserSuccess(response.data.user));
          error.response.config.headers[
            'Authorization'
          ] = `Bearer ${response.data.accessToken}`;
          processQueue(null, response.data.accessToken);
          return instance(error.response.config);
        })
        .catch(error => {
          console.log('[TOKEN] ', error);
          console.log('[TOKEN] Redirecting to Login');
          history.replace('/login');
          processQueue(error, null);
          // store.dispatch(
          //   loginUserError({
          //     status: error.response.status,
          //     message: error.response.data.message,
          //   })
          // );
          return Promise.reject(error);
        })
        .finally(() => {
          createAxiosResponseInterceptor();
          isRefreshing = false;
        });
    }
  );
};
createAxiosResponseInterceptor();

export default instance;
