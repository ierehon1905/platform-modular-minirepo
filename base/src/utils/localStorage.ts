const storageAvailable = () => {
  try {
    const x = '__storage_test__';
    localStorage.setItem(x, x);
    localStorage.removeItem(x);
    return true;
  } catch (e) {
    return false;
  }
};

export const getFromLocalStorage = <T>(key: string): T | null => {
  if (!storageAvailable()) return null;
  return JSON.parse(localStorage.getItem(key) || (null as any));
};

export const removeFromLocalStorage = (key: string) => {
  if (!storageAvailable()) return false;
  localStorage.removeItem(key);
  return true;
};

export const setItemLocalStorage = (key: string, value: any) => {
  if (!storageAvailable()) return false;
  localStorage.setItem(key, JSON.stringify(value));
  return false;
};
