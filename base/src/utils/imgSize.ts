// TODO: consider removing duplicates from 'sberlesson'

export const imgSize = (src: string) => {
  const image = new Image();
  return new Promise<{ width: number; height: number }>(resolve => {
    image.onload = () => resolve({ width: image.width, height: image.height });
    image.src = src;
  });
};
