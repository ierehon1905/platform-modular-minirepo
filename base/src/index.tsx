import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { TopMenu } from './components/TopMenu';
import { Provider } from 'react-redux';

import './index.css';
import { store } from './store';

import { BrowserRouter, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LessonWrapper } from 'components/LessonWrapper';

declare let __webpack_public_path__: string;

export type LessonType = {
  name: string;
  'name.ru': string;
};

const App = () => {
  const [lessons, setLessons] = useState<LessonType[]>([]);
  const [currentLesson, setCurrentLesson] = useState<
    null | undefined | LessonType
  >(null);

  React.useEffect(() => {
    console.log({ __webpack_public_path__: __webpack_public_path__ });

    fetch('http://localhost:5000/')
      .then(res => res.json())
      .then(res => {
        setLessons(res);
      });
  }, []);

  return (
    <React.StrictMode>
      <Provider store={store}>
        <BrowserRouter>
          <TopMenu />
          <ul style={{ marginTop: 120 }}>
            {lessons.map(lesson => (
              <li key={lesson.name}>
                <Link
                  to={`/lessons/${lesson.name}`}
                  onClick={() => setCurrentLesson(lesson)}
                >
                  {lesson['name.ru']}
                </Link>
              </li>
            ))}
          </ul>
          <Switch>
            <Route
              path="/lessons/:lesson"
              render={({ match }) => {
                return <LessonWrapper name={match.params.lesson} />;
              }}
            />
            <Route render={() => <Redirect to="/" />} />
          </Switch>
        </BrowserRouter>
      </Provider>
    </React.StrictMode>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
