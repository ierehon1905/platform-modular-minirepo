import React from 'react';
import { ErrorBoundary } from 'components/ErrorBoundary';

export const LessonWrapper = (props: { name: string }) => {
  const { name } = props;
  const [Lesson, setLesson] = React.useState(undefined);

  React.useEffect(() => {
    console.log('Began fetch');
    fetch(`http://localhost:5000/lessons/${name}/bundle.js`)
      .then(res => res.text())
      .then(res => {
        return eval(res)();
      })
      .then(res => {
        console.log({ res });

        setLesson(() => res);
      });
  }, []);

  return Lesson ? (
    <ErrorBoundary>
      <Lesson />
    </ErrorBoundary>
  ) : null;
};
