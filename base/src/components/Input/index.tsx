import React, {
  PropsWithChildren,
  CSSProperties,
  forwardRef,
  ForwardRefRenderFunction,
  useEffect,
  ChangeEvent,
} from 'react';
import styled, { css } from 'styled-components';

import { colors, textColorByTheme, smartColor } from 'utils/themeColors';
import { lightenDarkenColor, smartGrayColor } from '../../utils/themeColors';
import Icon, { IconTypes } from 'components/Icon';

export type InputProps = {
  iconLeft?: IconTypes;
  iconRight?: IconTypes;
  theme?: 'light' | 'dark' | 'accent' | 'current';
  size?: 's' | 'm' | 'l' | 'xl' | 'xxl' | 'xxxl';
  disabled?: boolean;
  title?: string;
  style?: CSSProperties;
  className?: string;
  label?: string;
  placeholder?: string;
  required?: boolean;
  autoComplete?: string;
  type?: string;
  name?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  value?: string;
};

const paddingMap: {
  [key in Exclude<InputProps['size'], undefined>]: number;
} = {
  s: 4,
  m: 8,
  l: 10,
  xl: 14,
  xxl: 16,
  xxxl: 18,
};

const backgroundColorMap: {
  [key in Exclude<InputProps['theme'], undefined>]: string;
} = {
  light: colors.lightBg,
  dark: colors.darkBg,
  accent: colors.accentBlueBg,
  current: colors.currentBg,
};
const borderColorMap: {
  [key in Exclude<InputProps['theme'], undefined>]: string;
} = {
  light: colors.darkBg,
  dark: colors.lightBg,
  accent: colors.accentBlueBg,
  current: colors.currentBg,
};
const focusBorderColorMap: {
  [key in Exclude<InputProps['theme'], undefined>]: string;
} = {
  light: colors.accentBlueBg,
  dark: colors.accentBlueBg,
  accent: colors.accentBlueBg,
  current: colors.accentBlueBg,
};

type StyledInputProps = {
  backgroundColor: string;
  borderColor: string;
  focusBorderColor: string;
  color: string;
  padding: number;
  disabled?: boolean;
  Size?: InputProps['size'];
  placeholder?: string;
  iconLeft?: IconTypes;
  iconRight?: IconTypes;
};

const BORDER_WIDTH = 2;

const StyledInput = styled.input<StyledInputProps>`
  position: relative;
  background: none;
  border: none;
  width: 100%;
  padding: ${({ padding }) => padding}px 16px;
  ${({ iconLeft }) =>
    iconLeft &&
    css`
      padding-left: 80px;
    `}

  ${({ iconRight }) =>
    iconRight &&
    css`
      padding-right: 80px;
    `}
  background-color: ${({ backgroundColor }) => backgroundColor};
  color: ${({ color }) => color};
  border: ${({ borderColor }) => `${BORDER_WIDTH}px solid ${borderColor}`};
  border-radius: ${({ Size: size }) => (size === 'xl' ? 19 : 16)}px;
  display: inline-flex;
  align-items: center;
  font-weight: 600;
  font-size: ${({ Size: size }) => (size === 'xl' ? 26 : 18)}px;
  min-height: 48px;
  /* cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')}; */
  opacity: ${({ disabled }) => (disabled ? 0.4 : 1)};
  transition: 0.2s ease-out;
  outline: none;
  &::placeholder {
    color: ${({ borderColor }) => lightenDarkenColor(borderColor, -20)};
    transition: 0.2s ease-out;
  }
  &:hover {
    border: ${({ borderColor }) =>
      `${BORDER_WIDTH}px solid ${lightenDarkenColor(borderColor, -50)}`};

    &::placeholder {
      color: ${({ borderColor }) => lightenDarkenColor(borderColor, -70)};
    }
  }
  &:valid {
    border: ${({ borderColor }) =>
      `${BORDER_WIDTH}px solid ${lightenDarkenColor(borderColor, -150)}`};
  }
  &:focus {
    border: ${({ focusBorderColor }) =>
      `${BORDER_WIDTH}px solid ${focusBorderColor}`};
    background-color: ${({ backgroundColor }) =>
      lightenDarkenColor(backgroundColor, 20)};
  }
`;

const coloredIcons: IconTypes[] = [
  'close-red',
  'helmet-colored',
  'forum-colored',
];

const InputEl: ForwardRefRenderFunction<HTMLInputElement, InputProps> = (
  props: InputProps,
  ref
) => {
  const { style, className, theme } = props;
  let backgroundColor = colors.pageBg;
  let borderColor = colors.lightBorder;
  let focusBorderColor = colors.accentBlueBg;
  let color = textColorByTheme(theme);
  let padding = 14;
  if (theme) {
    backgroundColor = backgroundColorMap[theme];
    borderColor = borderColorMap[theme];
    focusBorderColor = focusBorderColorMap[theme];
  }
  if (props.size) {
    padding = paddingMap[props.size];
  }

  return (
    <label style={{ marginTop: 20 }}>
      {props.iconLeft && (
        <>
          <Icon
            glyph={props.iconLeft as any}
            color={color}
            isColored={coloredIcons.includes(props.iconLeft)}
          />
        </>
      )}
      {props.label ||
        (props.placeholder && (
          <span
            style={{ display: 'block', padding: '16px 0 4px 0' }}
            className={!props.label ? 'sr-only' : ''}
          >
            {props.label || props.placeholder}
          </span>
        ))}
      {props.iconRight && (
        <>
          <Icon
            glyph={props.iconRight as any}
            color={color}
            isColored={coloredIcons.includes(props.iconRight)}
          />
        </>
      )}
      <StyledInput
        focusBorderColor={focusBorderColor}
        backgroundColor={backgroundColor}
        borderColor={borderColor}
        color={color}
        padding={padding}
        style={style}
        className={className}
        ref={ref}
        title={props.title}
        required={props.required}
        iconLeft={props.iconLeft}
        iconRight={props.iconRight}
        disabled={props.disabled}
        placeholder={props.placeholder}
        Size={props.size as any}
        onChange={props.onChange}
        autoComplete={props.autoComplete}
        name={props.name}
        type={props.type}
        value={props.value}
      />
    </label>
  );
};

export const Input = forwardRef(InputEl);
