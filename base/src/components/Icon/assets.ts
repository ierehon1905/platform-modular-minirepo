import arrowLeft from 'images/button-icons/arrow-left.svg';
import arrowRight from 'images/button-icons/arrow-right.svg';
import arrowUp from 'images/button-icons/arrow-up.svg';
import arrowDown from 'images/button-icons/arrow-down.svg';
import arrowNext from 'images/button-icons/arrow-next.svg';
import scroll from 'images/button-icons/scroll.svg';
import zoom from 'images/button-icons/zoom.svg';
import sound0 from 'images/button-icons/sound-0.svg';
import sound1 from 'images/button-icons/sound-1.svg';
import sound2 from 'images/button-icons/sound-2.svg';
import circle from 'images/button-icons/circle.svg';
import menu from 'images/button-icons/menu.svg';
import alert from 'images/button-icons/alert.svg';
import search from 'images/button-icons/search.svg';
import chat from 'images/button-icons/chat.svg';
import time from 'images/button-icons/time.svg';
import people from 'images/button-icons/people.svg';
import plus from 'images/button-icons/plus.svg';
import mapPoint from 'images/button-icons/map-point.svg';
import more from 'images/button-icons/more.svg';
import enter from 'images/button-icons/enter.svg';
import enterRight from 'images/button-icons/enter-right.svg';
import map1 from 'images/button-icons/map-1.svg';
import map2 from 'images/button-icons/map-2.svg';
import faq from 'images/button-icons/faq.svg';
import close from 'images/button-icons/close.svg';
import close_red from 'images/button-icons/close-red.svg';
import pirateHat from 'images/button-icons/pirate-hat.svg';
import circleFilled from 'images/button-icons/circle-filled.svg';
import mapPointFilled from 'images/button-icons/map-point-filled.svg';
import lockedPadlock from 'images/button-icons/locked-padlock.svg';
import unlockedPadlock from 'images/button-icons/unlocked-padlock.svg';
import directionSings from 'images/button-icons/direction-signs.svg';
import washingMachine from 'images/button-icons/washing-machine.svg';
import spoonFork from 'images/button-icons/spoon-fork.svg';
import shoppingCart from 'images/button-icons/shopping-cart.svg';
import speech from 'images/button-icons/speech.svg';
import name from 'images/button-icons/name.svg';
import tag from 'images/button-icons/tag.svg';
import cards from 'images/button-icons/cards.svg';
import coliseum from 'images/button-icons/coliseum.svg';
import helmet from 'images/button-icons/helmet.svg';
import boat from 'images/button-icons/boat.svg';
import sabre from 'images/button-icons/sabre.svg';
import home from 'images/button-icons/home.svg';
import brickWall from 'images/button-icons/brick-wall.svg';
import forumColored from 'images/button-icons/forum-colored.svg';
import helmetColored from 'images/button-icons/helmet-colored.svg';
import song from 'images/button-icons/song.svg';
import projector from 'images/button-icons/projector.svg';
import threeD from 'images/button-icons/3D.svg';
import check from 'images/button-icons/check.svg';
import clothes from 'images/button-icons/clothes.svg';
import DIY from 'images/button-icons/DIY.svg';
import education from 'images/button-icons/education.svg';
import game from 'images/button-icons/game.svg';
import holiday from 'images/button-icons/holiday.svg';
import theater from 'images/button-icons/theater.svg';
import user from 'images/button-icons/user.svg';
import fire from 'images/button-icons/fire.svg';
import cat from 'images/button-icons/cat.svg';
import fry from 'images/button-icons/fry.svg';
import oil from 'images/button-icons/oil.svg';
import goInside from 'images/button-icons/go-inside.svg';


export const mapping = {
  'arrow-left': arrowLeft,
  'arrow-right': arrowRight,
  'arrow-up': arrowUp,
  'arrow-down': arrowDown,
  'arrow-next': arrowNext,
  scroll,
  zoom,
  'sound-0': sound0,
  'sound-1': sound1,
  'sound-2': sound2,
  circle,
  menu,
  alert,
  search,
  chat,
  time,
  people,
  plus,
  'map-point': mapPoint,
  more,
  enter,
  'enter-right': enterRight,
  'map-1': map1,
  'map-2': map2,
  faq,
  cat,
  fry,
  oil,
  close,
  'close-red': close_red,
  'pirate-hat': pirateHat,
  'circle-filled': circleFilled,
  'map-point-filled': mapPointFilled,
  'locked-padlock': lockedPadlock,
  'unlocked-padlock': unlockedPadlock,
  'direction-signs': directionSings,
  'washing-machine': washingMachine,
  'spoon-fork': spoonFork,
  'shopping-cart': shoppingCart,
  speech,
  name,
  tag,
  cards,
  coliseum,
  helmet,
  boat,
  sabre,
  home,
  'brick-wall': brickWall,
  'forum-colored': forumColored,
  'helmet-colored': helmetColored,
  song,
  projector,
  '3D': threeD,
  check,
  clothes,
  DIY,
  education,
  game,
  holiday,
  fire,
  theater,
  user,
  'go-inside': goInside,
};
