import styled, { css } from 'styled-components';
import { mapping } from './assets';

export type IconTypes = keyof typeof mapping;

type IconProps = {
  glyph: IconTypes;
  color?: string;
  size?: number;
  isColored?: boolean;
};

const Icon = styled.div<IconProps>`
  width: ${({ size }) => size || 40}px;
  height: ${({ size }) => size || 40}px;
  ${({ glyph, color, isColored }) => {
    if (isColored) {
      return css`
        background-image: url(${mapping[glyph]});
        background-size: cover;
      `;
    }
    return css`
      background-color: ${color};
      mask: url(${mapping[glyph]});
    `;
  }};
  -webkit-mask-repeat-x: no-repeat;
  -webkit-mask-repeat-y: no-repeat;
  mask-repeat: no-repeat;
  mask-size: contain;
`;

export default Icon;
