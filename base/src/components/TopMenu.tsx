import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Button, ButtonProps } from './Button';
import { Breadcrumbs } from './Breadcrumbs';

const HEADER_HEIGHT = 120;

const Wrapper = styled.header`
  padding: 0 64px;
  height: ${HEADER_HEIGHT}px;
  display: inline-flex;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  z-index: 10;
  /* background-color: rgba(242, 242, 242, 0);
  backdrop-filter: blur(20px);
  -webkit-mask-image: linear-gradient(
    180deg,
    black 0%,
    black 50%,
    black 70%,
    transparent 100%
  ); */
`;

const ButtonRow = styled.div`
  flex-grow: 1;
  text-align: right;
  & > * {
    margin-right: 16px;
  }
`;

const AvatarInfo = styled.div`
  height: 72px;
  cursor: pointer;
  & > img {
    margin-left: 20px;
    height: inherit;
  }
`;
const AvatarImage = styled.img`
  border-radius: 9999px;
  border: 3px solid #e1e3e7;
`;

const navButtons: { icon: ButtonProps['icon']; link?: string }[] = [
  {
    icon: 'chat',
    link: '/chats',
  },
  {
    icon: 'time',
    link: '/schedule',
  },
  {
    icon: 'people',
    link: '/contacts',
  },
  {
    icon: 'plus',
  },
];

export const TopMenu: React.FC<any> = props => {
  return (
    <Wrapper>
      <span
        style={{
          fontFamily: 'Ubuntu',
          fontSize: 30,
          color: '#3C3D46',
          cursor: 'pointer',
        }}
      >
        Ad Astrum
      </span>
      <div
        style={{
          width: 2,
          borderRadius: 10,
          height: 40,
          backgroundColor: '#3C3D46',
          margin: '0 40px',
        }}
      ></div>
      {props.isBreadcrumbsMode ? (
        <Breadcrumbs items={props.breadcrumbs as string[]} />
      ) : (
        <>
          <Button
            icon="search"
            size="s"
            onClick={() => props.onSearch()}
            style={{ opacity: 0 }}
          ></Button>

          <ButtonRow>
            {navButtons.map((item, index) => {
              return <Button key={index} icon={item.icon} size="s" />;
            })}
            <Button
              theme={props.isMenuVisible ? 'accent' : 'light'}
              icon="menu"
              size="s"
              onClick={() => {
                props.isMenuVisible ? props.onMenuHide() : props.onMenuShow();
              }}
            />
            <span style={{ marginLeft: '1em' }}>
              {/* <Button {...getNavButtonProps({ icon: "alert" })} /> */}
            </span>
          </ButtonRow>
          {/* <AvatarInfo onClick={() => navigateTo("/profile")}>
                        <AvatarImage
                            src={getGravatar(props.user?.email)}
                            alt="avatar"
                        />
                    </AvatarInfo> */}
          {/* <Modal
                        open={props.isMenuVisible}
                        top={"0"}
                        left={"0"}
                        width={"100%"}
                        height={"100%"}
                        background={"#f2f2f2"}>
                        <MainMenu onClose={props.onMenuHide} />
                       
                    </Modal> */}
        </>
      )}
    </Wrapper>
  );
};
