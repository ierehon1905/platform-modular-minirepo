import React, {
  PropsWithChildren,
  CSSProperties,
  forwardRef,
  ForwardRefRenderFunction,
  useEffect,
} from 'react';
import styled, { css } from 'styled-components';

import {
  colors,
  textColorByTheme,
  smartColor,
  getRGBA,
} from 'utils/themeColors';
import { lightenDarkenColor, smartGrayColor, isDark } from 'utils/themeColors';
import Icon, { IconTypes } from 'components/Icon';

export type ButtonProps = PropsWithChildren<{
  icon?: IconTypes;
  iconLeft?: IconTypes;
  iconRight?: IconTypes;
  theme?: 'light' | 'dark' | 'accent' | 'current';
  view?: 'pseudo' | 'action';
  size?: 's' | 'm' | 'l' | 'xl' | 'xxl' | 'xxxl';
  onClick?: () => void;
  disabled?: boolean;
  shadow?: boolean;
  title?: string;
  style?: CSSProperties;
  className?: string;
  keyListener?: string;
}>;

const paddingMap: {
  [key in Exclude<ButtonProps['size'], undefined>]: number;
} = {
  s: 4,
  m: 8,
  l: 10,
  xl: 14,
  xxl: 16,
  xxxl: 18,
};

const bgColorMap: {
  [key in Exclude<ButtonProps['theme'], undefined>]: string;
} = {
  light: colors.lightBg,
  dark: colors.darkBg,
  accent: colors.accentBlueBg,
  current: colors.currentBg,
};

type StyledButtonProps = {
  bgColor: string;
  color: string;
  padding: number;
  disabled?: boolean;
  shadow?: boolean;
  borderWidth?: number;
  size?: ButtonProps['size'];
};

const BORDER_WIDTH = 3;

const StyledButton = styled.button<StyledButtonProps>`
  position: relative;
  background: none;
  border: none;
  padding: ${({ padding }) => padding}px;
  background-color: ${({ bgColor }) => bgColor};
  color: ${({ color }) => color};
  border: ${({ color, borderWidth }) =>
    `${borderWidth && borderWidth > 0 ? borderWidth : BORDER_WIDTH}px solid ${
      borderWidth && borderWidth > 0 ? color : 'transparent'
    }`};
  border-radius: ${({ size }) =>
    size === 'xxl' || size === 'xxxl' ? 19 : 16}px;
  display: inline-flex;
  align-items: center;
  font-weight: 600;
  font-size: ${({ size }) => (size === 'xxl' || size === 'xxxl' ? 26 : 18)}px;
  min-height: 48px;
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
  opacity: ${({ disabled }) => (disabled ? 0.4 : 1)};
  pointer-events: ${({ disabled }) => (disabled ? 'none' : 'all')};
  transition: 0.2s ease-out;
  ${({ shadow, bgColor }) =>
    shadow &&
    css`
      box-shadow: ${isDark(bgColor)
        ? `0px 16px 32px ${getRGBA(bgColor, 0.1)}`
        : `8px 24px 42px rgba(0, 0, 0, 0.22)`};
    `}
  outline: none;
  &:hover {
    border: ${({ color, borderWidth }) =>
      `${borderWidth && borderWidth > 0 ? borderWidth : BORDER_WIDTH}px solid ${
        borderWidth && borderWidth > 0
          ? lightenDarkenColor(color, 20)
          : 'transparent'
      }`};
    background-color: ${({ bgColor }) => lightenDarkenColor(bgColor, 20)};
    box-shadow: ${({ bgColor }) =>
      isDark(bgColor)
        ? `0px 16px 32px ${getRGBA(bgColor, 0.3)}`
        : `8px 24px 42px rgba(0, 0, 0, 0.32)`};
  }
  &:focus {
    ${({ bgColor, borderWidth }) =>
      smartGrayColor(bgColor) != bgColor &&
      css`
        border: ${`${
          borderWidth && borderWidth > 0 ? borderWidth : BORDER_WIDTH
        }px solid ${smartGrayColor(bgColor)}`};
      `}
    background-color: ${({ bgColor }) => lightenDarkenColor(bgColor, -10)};
  }
`;

const coloredIcons: IconTypes[] = [
  'close-red',
  'helmet-colored',
  'forum-colored',
];

const Divider = styled.hr<{ color: string }>`
  border: none;
  /* TODO под size редачить */
  height: 24px;
  width: 1px;
  background-color: ${({ color }) => color};
  margin: 0 6px;
  opacity: 0.2;
`;

const ButtonEl: ForwardRefRenderFunction<HTMLButtonElement, ButtonProps> = (
  props: ButtonProps,
  ref
) => {
  const { style, className, keyListener, onClick, theme } = props;
  let bgColor = colors.lightBg;
  let color = textColorByTheme(theme);
  let padding = paddingMap.s;
  let border = 0;
  if (theme) {
    bgColor = bgColorMap[theme];
  }
  if (props.view === 'pseudo') {
    color = bgColor;
    bgColor = 'transparent';
    border = BORDER_WIDTH;
  }
  if (props.size) {
    padding = paddingMap[props.size];
  }
  const hasContent = !!props.children;

  useEffect(() => {
    if (keyListener && onClick) {
      const onKeyUp = (event: KeyboardEvent) => {
        // @ts-ignore
        if (event.code === keyListener && event.target.nodeName !== 'INPUT') {
          event.preventDefault();
          onClick();
        }
      };
      window.addEventListener('keydown', onKeyUp);
      return () => {
        window.removeEventListener('keydown', onKeyUp);
      };
    }
  }, [onClick, keyListener]);

  return (
    <StyledButton
      borderWidth={border}
      bgColor={bgColor}
      color={color}
      padding={padding}
      onClick={onClick}
      disabled={props.disabled}
      shadow={props.shadow}
      title={props.title}
      style={style}
      className={className}
      ref={ref}
      size={props.size as any}
    >
      {props.iconLeft && (
        <>
          <Icon
            glyph={props.iconLeft as any}
            color={color}
            isColored={coloredIcons.includes(props.iconLeft)}
          />
          {(hasContent || props.iconRight) && <Divider color={color} />}
        </>
      )}
      {hasContent && (
        <span style={{ padding: '0 10px' }}>{props.children}</span>
      )}
      {(props.icon || props.iconRight) && (
        <>
          {hasContent && <Divider color={color} />}
          <Icon
            glyph={(props.icon || props.iconRight) as any}
            color={color}
            isColored={coloredIcons.includes(
              (props.icon || props.iconRight) as any
            )}
          />
        </>
      )}
    </StyledButton>
  );
};

export const Button = forwardRef(ButtonEl);
