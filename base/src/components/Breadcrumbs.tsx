import React from 'react';
import styled from 'styled-components';
import Icon from './Icon';
const HEADER_HEIGHT = 120;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  height: ${HEADER_HEIGHT}px;
  z-index: 10;
  margin: 0;
`;
const Inner = styled.div`
  display: flex;
  align-items: center;
  z-index: 10;
  margin: 0;
  padding: 8px 20px 10px 20px;
  border-radius: 16px;
  /* background-color: rgba(242, 242, 242, 0.6);
  backdrop-filter: blur(20px); */
`;

const NavItem = styled.div<{ active: boolean }>`
  white-space: nowrap;
  font-size: 20px;
  color: ${({ active }) => (active ? '#343A45' : '#7D7F8A')};
  height: 20px;
  margin: 10px 0;
  /* cursor: ${({ active }) => (active ? 'default' : 'pointer')}; */
  cursor: default;
`;

export type BreadcrumbsProps = {
  items: string[];
};

export const Breadcrumbs: React.FC<BreadcrumbsProps> = props => {
  return (
    <Wrapper>
      <Inner>
        {props.items.map((item, index, items) => (
          <NavItem key={index.toString()} active={items.length === index + 1}>
            {item}
            {items.length !== index + 1 && (
              <Icon
                size={12}
                color="#B5B9C7"
                glyph={'arrow-next'}
                style={{
                  display: 'inline-block',
                  margin: '0 12px',
                }}
              />
            )}
          </NavItem>
        ))}
      </Inner>
    </Wrapper>
  );
};
