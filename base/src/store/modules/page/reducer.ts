import { Reducer } from 'redux';
import { pageState } from './types';
import * as A from './constants';
import { enableBreadcrumbs } from './actions';

const initialState: pageState = {
  location: null,
  openModal: null,
  isMenuVisible: false,
  isTopMenuVisible: true,
  isBreadcrumbsMode: true,
  breadcrumbs: null,
};

export const pageReducer: Reducer<pageState> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case A.MODAL_OPEN: {
      return { ...state, openModal: action.payload };
    }
    case A.MODAL_CLOSE: {
      return { ...state, openModal: null };
    }

    case A.SHOW_MENU: {
      return { ...state, isMenuVisible: true };
    }
    case A.HIDE_MENU: {
      return { ...state, isMenuVisible: false };
    }

    case A.SHOW_HEADER: {
      return { ...state, isTopMenuVisible: true };
    }
    case A.HIDE_HEADER: {
      return { ...state, isTopMenuVisible: false };
    }

    case A.ENABLE_BREAD_CRUMBS_MODE: {
      const { breadcrumbs } = action as ReturnType<typeof enableBreadcrumbs>;

      return { ...state, isBreadcrumbsMode: true, breadcrumbs };
    }
    case A.DISABLE_BREAD_CRUMBS_MODE: {
      return { ...state, isBreadcrumbsMode: false };
    }

    default:
      return state;
  }
};
