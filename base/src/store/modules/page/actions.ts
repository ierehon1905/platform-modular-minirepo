import * as C from './constants';

export const openModal = (modalName: string) => ({
  type: C.MODAL_OPEN,
  payload: modalName,
});

export const openClose = (modalName: string) => ({
  type: C.MODAL_CLOSE,
  payload: modalName,
});

export const showMenu = () => ({
  type: C.SHOW_MENU,
});

export const hideMenu = () => ({
  type: C.HIDE_MENU,
});

export const showHeader = () => ({
  type: C.SHOW_HEADER,
});

export const hideHeader = () => ({
  type: C.HIDE_HEADER,
});

export const disableBreadcrumbs = () => ({
  type: C.DISABLE_BREAD_CRUMBS_MODE,
});

export const enableBreadcrumbs = (breadcrumbs: string[]) => ({
  type: C.ENABLE_BREAD_CRUMBS_MODE,
  breadcrumbs,
});
