export type pageState = {
  location?: string | null;
  // TODO возможно когда-нибудь будет открытие нескольких модалок
  openModal?: string | null;
  /**
   * Грид
   */
  isMenuVisible: boolean;
  /**
   * Хедер
   */
  isTopMenuVisible: boolean;
  /**
   * Режим хлебных крошек. Кнопки меню и аватар скрыты.
   */
  isBreadcrumbsMode: boolean;
  /**
   * Сами хлебные крошки
   */
  breadcrumbs: null | string[];
};
