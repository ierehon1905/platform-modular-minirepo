import { takeLatest, all } from 'redux-saga/effects';
import { PAGE_LOADED } from './constants';

function* pageLoaded() {}

export function* watchPageLoad() {
  yield takeLatest(PAGE_LOADED, pageLoaded);
}

export function* commonSaga() {
  yield all([watchPageLoad()]);
}
