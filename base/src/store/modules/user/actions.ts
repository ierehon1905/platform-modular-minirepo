import {
  LOGIN_USER_START,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  FETCH_USER_START,
  LOGOUT_USER,
  FETCH_GRAPH_START,
  FETCH_GRAPH_SUCCESS,
  FETCH_GRAPH_ERROR,
  FETCH_SET_PROGRESS_START,
  FETCH_SET_PROGRESS_SUCCESS,
  FETCH_SET_PROGRESS_ERROR,
  RESET_GRAPH,
  ADD_ACHIEVEMENT,
  SHOW_ACHIEVEMENT,
  CHECK_ACHIEVEMENT,
  SET_ACCESS_TOKEN,
  SET_REFRESH_TOKEN,
  VERIFY_USER_START,
  REGISTER_USER_START,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
} from './constants';
import { ILoginData, IUserData, IRegisterData, IApiError } from './types';

export const loginUserStart = (data: ILoginData) => ({
  type: LOGIN_USER_START,
  payload: data,
});

export const loginUserSuccess = (user: IUserData) => ({
  type: LOGIN_USER_SUCCESS,
  payload: user,
});

export const loginUserError = (error: IApiError) => ({
  type: LOGIN_USER_ERROR,
  payload: error,
});

export const fetchUser = () => ({
  type: FETCH_USER_START,
});

export const registerUserStart = (user: IRegisterData) => ({
  type: REGISTER_USER_START,
  payload: user,
});
export const registerUserSuccess = () => ({
  type: REGISTER_USER_SUCCESS,
});
export const registerUserError = (error: IApiError) => ({
  type: REGISTER_USER_ERROR,
  payload: error,
});

export const logoutUser = () => ({
  type: LOGOUT_USER,
  payload: null,
});

export const setAccessToken = (accessToken: string) => ({
  type: SET_ACCESS_TOKEN,
  accessToken,
});

export const setRefreshToken = (refreshToken: string) => ({
  type: SET_REFRESH_TOKEN,
  refreshToken,
});

export const verifyUserStart = (verifyToken: string) => ({
  type: VERIFY_USER_START,
  verifyToken,
});
