export interface IApiError {
  message: string;
  status: number;
}

export interface IRegisterData {
  email: string;
  username: string;
  password: string;
}

export interface ILoginData {
  email: string;
  password: string;
}

export type IUserData = IUserLoginResponse['user'];

export interface IUserState {
  user: IUserData | null;
  loading: boolean;
  error: IApiError | null;
  accessToken: string | null;
  refreshToken: string | null;
}

export interface IUserLoginResponse {
  accessToken: string;
  refreshToken: string;
  user: {
    id: string;
    username: string;
    role: 'USER';
    email: string;
  };
}
