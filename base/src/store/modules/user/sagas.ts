import {
  takeLatest,
  call,
  put,
  all,
  take,
  fork,
  cancel,
} from 'redux-saga/effects';
import {
  loginUserStart,
  loginUserSuccess,
  loginUserError,
  setAccessToken,
  setRefreshToken,
  verifyUserStart,
  registerUserStart,
  registerUserSuccess,
  registerUserError,
} from './actions';
import {
  LOGIN_USER_START,
  FETCH_USER_START,
  LOGOUT_USER,
  VERIFY_USER_START,
  REGISTER_USER_START,
  LOGIN_USER_ERROR,
} from './constants';
import {
  login,
  fetchCurrentUser,
  verify,
  register,
} from './service';
import { IUserLoginResponse } from './types';

import { Task } from 'redux-saga';

function* registerUserSaga(action: ReturnType<typeof registerUserStart>) {
  try {
    yield call(register, action.payload);
    yield put(registerUserSuccess());
  } catch (e) {
    yield put(registerUserError(e));
  }
}

function* watchRegisterUser() {
  yield takeLatest(REGISTER_USER_START, registerUserSaga);
}

function* loginUserSaga(action: ReturnType<typeof loginUserStart>) {
  console.log('[TOKEN] Trying assign tokens ');
  try {
    const userLogin: IUserLoginResponse = yield call(login, action.payload);
    yield put(setAccessToken(userLogin.accessToken));
    yield put(setRefreshToken(userLogin.refreshToken));
    yield put(loginUserSuccess(userLogin.user));
  } catch (e) {
    console.error('[TOKEN] Error while trying assign tokens', e);
    yield put(loginUserError(e));
  }
}

// function* watchLoginUserSaga() {
//   yield takeLatest(LOGIN_USER_START, loginUserSaga);
// }

function* fetchUserSaga() {
  try {
    const response = yield call(fetchCurrentUser);
    yield put(loginUserSuccess(response));
  } catch (error) {
    yield put(
      loginUserError({
        status: error?.status || 404,
        message: 'Пользователь не найден',
      })
    );
  }
}


function* verifyUser({ verifyToken }: ReturnType<typeof verifyUserStart>) {
  try {
    const userLogin: IUserLoginResponse = yield call(verify, verifyToken);
    yield put(setAccessToken(userLogin.accessToken));
    yield put(setRefreshToken(userLogin.refreshToken));
    yield put(loginUserSuccess(userLogin.user));
    // history.replace('/login');
  } catch (e) {
    yield put(loginUserError(e));
  }
}


function* logoutUserSaga() {
  try {
    yield put(setAccessToken(''));
    yield put(setRefreshToken(''));
  } catch (error) {
    yield put(loginUserError(error));
  }
}

function* watchLogoutUser() {
  yield takeLatest(LOGOUT_USER, logoutUserSaga);
}

const actionMap = {
  [VERIFY_USER_START]: verifyUser,
  [LOGIN_USER_START]: loginUserSaga,
  [FETCH_USER_START]: fetchUserSaga,
};
function* manageUserAuthentication() {
  let task: Task | undefined;
  while (true) {
    const action = yield take([
      VERIFY_USER_START,
      LOGIN_USER_START,
      FETCH_USER_START,
      LOGIN_USER_ERROR,
    ]);
    if (action.type === LOGIN_USER_ERROR) {
      // TODO redirect
      // yield put(push('/login'));
    } else if (action.type === VERIFY_USER_START) {
      if (task) yield cancel(task);
      yield call(actionMap[action.type], action);
    } else {
      task = yield fork(actionMap[action.type], action);
    }
  }
}

export function* userSaga() {
  yield all([
    manageUserAuthentication(),
    watchLogoutUser(),
    watchRegisterUser(),
  ]);
}
