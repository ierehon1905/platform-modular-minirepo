import axios from 'utils/axios';
import {
  ILoginData,
  IUserLoginResponse,
  IUserData,
  IRegisterData,
} from './types';

export async function register(
  payload: IRegisterData
): Promise<IUserLoginResponse> {
  return axios.post('/auth/registration', { ...payload });
}

export async function login(payload: ILoginData): Promise<IUserLoginResponse> {
  const { email, password } = payload;
  const { data } = await axios.post('/auth/login', { email, password });

  return data;
}

export async function verify(verifyToken: string): Promise<IUserLoginResponse> {
  const { data } = await axios.put('/auth/verify', { token: verifyToken });
  return data;
}

export async function fetchCurrentUser(): Promise<IUserData> {
  const { data } = await axios.get('/users/me');
  return data;
}
