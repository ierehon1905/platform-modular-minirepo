import { Reducer } from 'redux';
import { set } from 'lodash/fp';
import {
  LOGIN_USER_START,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  FETCH_USER_START,
  LOGOUT_USER,
  FETCH_GRAPH_START,
  FETCH_GRAPH_SUCCESS,
  FETCH_GRAPH_ERROR,
  RESET_GRAPH,
  ADD_ACHIEVEMENT,
  SHOW_ACHIEVEMENT,
  SET_ACCESS_TOKEN,
  SET_REFRESH_TOKEN,
  REGISTER_USER_START,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
} from './constants';
import { IUserState } from './types';
import { getFromLocalStorage, setItemLocalStorage } from 'utils/localStorage';
import {
  loginUserSuccess,
  registerUserError,
} from './actions';

const initialState: IUserState = {
  user: null,
  loading: false,
  error: null,
  accessToken: getFromLocalStorage('accessToken'),
  refreshToken: getFromLocalStorage('refreshToken'),
};

export const userReducer: Reducer<IUserState> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case LOGIN_USER_START:
    case REGISTER_USER_START:
    case FETCH_USER_START:
      return { ...state, loading: true, error: null };
    case LOGIN_USER_SUCCESS: {
      const { payload } = action as ReturnType<typeof loginUserSuccess>;
      return {
        ...state,
        loading: false,
        user: payload,
        error: null,
      };
    }
    case REGISTER_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: null,
      };
    }
    case REGISTER_USER_ERROR: {
      const { payload } = action as ReturnType<typeof registerUserError>;
      return {
        ...state,
        loading: false,
        error: payload,
      };
    }
    case LOGOUT_USER:
      return { ...state, loading: false, user: action.payload };
    case LOGIN_USER_ERROR:
      return { ...state, loading: false, error: action.payload, user: null };
    case SET_ACCESS_TOKEN: {
      const { accessToken } = action;
      setItemLocalStorage('accessToken', accessToken);
      // axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
      return { ...state, accessToken };
    }
    case SET_REFRESH_TOKEN: {
      const { refreshToken } = action;
      setItemLocalStorage('refreshToken', refreshToken);
      return { ...state, refreshToken };
    }
    default:
      return state;
  }
};
