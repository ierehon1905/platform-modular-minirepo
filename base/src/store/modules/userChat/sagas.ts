import { USER_CHAT_TRANSFORM_NEW_MESSAGE } from './const';
import { addNewMessage, transformNewMessage } from './actions';
import { put, takeLatest, all, call, select } from 'redux-saga/effects';

function* transformMessage(action: ReturnType<typeof transformNewMessage>) {
  const { userChatState } = yield select();
  const { messages } = userChatState.room;
  const newID = messages.length > 0 ? messages[messages.length - 1].id + 1 : 0;
  const { payload } = yield call(transformNewMessage, action.payload);
  try {
    const newMessage = { ...payload, status: 'sending', id: newID };
    yield put(addNewMessage(newMessage));
  } catch (err) {
    const { payload } = yield call(transformNewMessage, action.payload);
    yield put(addNewMessage({ ...payload, status: 'error', id: newID }));
  }
}

export function* watchNewMessage() {
  yield takeLatest(USER_CHAT_TRANSFORM_NEW_MESSAGE, transformMessage);
}

export function* userChatSaga() {
  yield all([watchNewMessage()]);
}
