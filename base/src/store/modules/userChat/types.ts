export interface IRoomItem {
  coverImage?: string;
  name: string;
  users: number;
}

export interface IDialogItem {
  name: string;
  isOnline: boolean;
}

export interface IAttachment {
  title: string;
  type: 'file' | 'image' | 'video';
  src: string;
}

export interface IMessage {
  text?: string;
  date: Date;
  user: string;
  status: 'sending' | 'delivered' | 'read' | 'error';
  id: string;
  attachments?: IAttachment[];
}

export interface IUserChatState {
  rooms: IRoomItem[];
  dialogues: IDialogItem[];
  room: {
    name: string;
    messages: IMessage[];
  };
}
