import { Reducer } from 'redux';
import { IUserChatState } from './types';
import { USER_CHAT_ADD_NEW_MESSAGE } from './const';

const initialState: IUserChatState = {
  rooms: [],
  dialogues: [],
  room: {
    name: '',
    messages: [],
  },
};

export const userChatReducer: Reducer<IUserChatState> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case USER_CHAT_ADD_NEW_MESSAGE:
      return {
        ...state,
        room: {
          ...state.room,
          messages: [...state.room.messages, action.payload],
        },
      };
    default:
      return state;
  }
};
