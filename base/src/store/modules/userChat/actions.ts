interface IAttachmentRaw extends Omit<IAttachment, 'title'> {
  file: File;
}

export interface IMessageRaw
  extends Omit<IMessage, 'attachments' | 'id' | 'status'> {
  attachments?: IAttachmentRaw[];
}

import {
  USER_CHAT_ADD_NEW_MESSAGE,
  USER_CHAT_TRANSFORM_NEW_MESSAGE,
} from './const';
import { IAttachment, IMessage } from './types';

export const transformNewMessage = (message: IMessageRaw) => ({
  type: USER_CHAT_TRANSFORM_NEW_MESSAGE,
  payload: message,
});

export const addNewMessage = (message: IMessage) => ({
  type: USER_CHAT_ADD_NEW_MESSAGE,
  payload: message,
});
