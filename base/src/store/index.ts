import { combineReducers, applyMiddleware } from 'redux';
import { all } from 'redux-saga/effects';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history';
import createSagaMiddleware from 'redux-saga';

// User Module
import { userReducer } from './modules/user/reducer';
import { userChatReducer } from './modules/userChat/reducer';
import { userSaga } from './modules/user/sagas';
import { pageReducer } from './modules/page/reducer';
import { commonSaga } from './modules/common/sagas';
import { userChatSaga } from './modules/userChat/sagas';
import { IUserState } from './modules/user/types';
import { IModule, createStore, IModuleStore } from 'redux-dynamic-modules';
import { ISagaModule, getSagaExtension } from 'redux-dynamic-modules-saga';
// import { createStore, IModuleStore } from "redux-dynamic-modules";

function rootModule(): ISagaModule<IStore> {
  return {
    id: 'root',
    reducerMap: {
      userState: userReducer,
    },
    sagas: [userSaga],
  };
}

const createRootReducer = history =>
  combineReducers({
    // router: connectRouter(history),
    userState: userReducer,
    // pageState: pageReducer,
    // userChatState: userChatReducer,
    // новые модули добавляются тут
  });

export type IStore = ReturnType<ReturnType<typeof createRootReducer>>;

const createRootSaga = () =>
  function* () {
    // Грязный прием глобального отлова ошибок в сагах
    while (true) {
      try {
        yield all([userSaga(), commonSaga(), userChatSaga()]);
      } catch (error) {
        console.error('GLOBAL ERROR ', error);
      }
    }
  };

export const history = createBrowserHistory();

export const store: IModuleStore<IStore> = createStore(
  {
    initialState: {
      /** initial state */
    },
    enhancers: [
      /** enhancers to include */
    ],
    extensions: [
      /** extensions to include i.e. getSagaExtension(), getThunkExtension() */
      getSagaExtension({}),
    ],
  },
  rootModule()
  /* ...any additional modules */
);

// const configureStore = () => {
//   const composeEnhancers = composeWithDevTools({});
//   const sagaMiddleware = createSagaMiddleware();

//   const store = createStore(
//     createRootReducer(history),
//     composeEnhancers(applyMiddleware(sagaMiddleware))
//   );

//   sagaMiddleware.run(createRootSaga());

//   return store;
// };

// export const store = configureStore();
