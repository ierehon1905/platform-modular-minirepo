const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = (env = {}) => ({
  entry: './src/index.tsx',
  mode: env.production ? 'production' : 'development',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: require.resolve('redux-dynamic-modules'),
        loader: 'expose-loader',
        options: {
          exposes: ['ReduxDynamicModules'],
        },
      },
      {
        test: require.resolve('redux-dynamic-modules-saga'),
        loader: 'expose-loader',
        options: {
          exposes: ['ReduxDynamicModulesSaga'],
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(svg|png|jpg)$/,
        use: {
          loader: 'url-loader',
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      react: env.production
        ? 'https://unpkg.com/react@16/umd/react.production.min.js'
        : 'https://unpkg.com/react@16/umd/react.development.js',
      reactDom: env.production
        ? 'https://unpkg.com/react-dom@16/umd/react-dom.production.min.js'
        : 'https://unpkg.com/react-dom@16/umd/react-dom.development.js',
      template: './public/index.ejs',
    }),
    new CompressionPlugin({
      algorithm: 'brotliCompress',
      compressionOptions: {
        // zlib’s `level` option matches Brotli’s `BROTLI_PARAM_QUALITY` option.
        level: 11,
      },
      deleteOriginalAssets: env.production || false,
    }),
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3000,
    historyApiFallback: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers':
        'X-Requested-With, content-type, Authorization',
    },
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.json', '.css'],
    alias: {
      images: path.resolve(__dirname, 'images/'),
      components: path.resolve(__dirname, 'src/components/'),
      utils: path.resolve(__dirname, 'src/utils/'),
    },
  },
  externals: [
    {
      react: 'React',
      'react-is': 'ReactIs',
      'react-dom': 'ReactDOM',
      'react-router-dom': 'ReactRouterDOM',
      redux: 'Redux',
      'react-redux': 'ReactRedux',
      'redux-saga/effects': 'ReduxSagaEffects',
      'redux-saga': 'ReduxSaga',
      'styled-components': 'styled',
    },
    require('webpack-externalize-lodash'),
  ],
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
  performance: {
    hints: process.env.NODE_ENV === 'production' ? 'warning' : false,
  },
});
