const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');
const DefinePlugin = require('webpack').DefinePlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const config = require('./public/lesson.config.json')

module.exports = {
  entry: './src/main.tsx',
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: `http://localhost:5000/lessons/${config.name}/`,
    libraryTarget: 'jsonp',
    libraryExport: 'default',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(svg|png|jpe?g|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
          // {
          //   loader: 'file-loader',
          //   options: {
          //     name: '[path][name].[ext][query]',
          //   },
          // },
        ],
      },
    ],
  },
  externals: [
    {
      react: 'React',
      'react-is': 'ReactIs',
      'react-dom': 'ReactDOM',
      'react-router-dom': 'ReactRouterDOM',
      'react-redux': 'ReactRedux',
      redux: 'Redux',
      'redux-dynamic-modules': 'ReduxDynamicModules',
      'redux-dynamic-modules-saga': 'ReduxDynamicModulesSaga',
      'redux-saga/effects': 'ReduxSagaEffects',
      'redux-saga': 'ReduxSaga',
      'styled-components': 'styled',
    },
    require('webpack-externalize-lodash'),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.json', '.css'],
  },
  plugins: [
    new DefinePlugin({
      config: JSON.stringify(require('./public/lesson.config.json')),
    }),
    new CopyWebpackPlugin({ patterns: [{ from: 'public/', to: '.' }] }),
    new CompressionPlugin({
      algorithm: 'brotliCompress',
      compressionOptions: {
        // zlib’s `level` option matches Brotli’s `BROTLI_PARAM_QUALITY` option.
        level: 11,
      },
    }),
    new ZipPlugin({
      path: '../zip',
      filename: 'packed_bundle',
    }),
  ],
  // optimization: {
  //   minimize: false,
  // },
};
