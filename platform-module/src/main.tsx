import React from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import { DynamicModuleLoader } from 'redux-dynamic-modules';

import { Main } from './pages/Main';
import { Sub } from './pages/Sub';
import { getLessonModule } from './module';


const LessonWrapper = async () => {
  // console.log({ __webpack_public_path__: __webpack_public_path__ });

  // __webpack_public_path__ = `http://localhost:5000/lessons/${config.name}/`;
  // console.log({ __webpack_public_path__: __webpack_public_path__ });

  // const Main = (await import('./pages/Main')).Main;
  // const Sub = (await import('./pages/Sub')).Sub;
  // const getLessonModule = (await import('./module')).getLessonModule;
  return () => {
    React.useEffect(() => {
      console.log('INSIDE MAIN_TEST/useEffect');
    }, []);

    return (
      <DynamicModuleLoader modules={[getLessonModule()]}>
        <div style={{ marginTop: 120 }}>
          <h1>Hello User, я из другой репы</h1>
          <div>
            <ul>
              <li>
                <Link to={`/lessons/${config.name}/main`}>main</Link>
              </li>
              <li>
                <Link to={`/lessons/${config.name}/sub`}>sub</Link>
              </li>
            </ul>
          </div>
          <Switch>
            <Route path={`/lessons/${config.name}/main`} component={Main} />
            <Route path={`/lessons/${config.name}/sub`} component={Sub} />
          </Switch>
        </div>
      </DynamicModuleLoader>
    );
  };
};
export default LessonWrapper;
