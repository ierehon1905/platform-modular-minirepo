const fs = require("fs")
const express = require("express")
const cors = require("cors")
const app = express()
const multer = require("multer")
const extract = require("extract-zip")
const path = require("path")

if (!fs.existsSync("./temp_upload")) {
    fs.mkdirSync("./temp_upload")
}

app.use(cors())
app.use("/lessons", express.static("lessons"))

app.get("/", (req, res) => {
    const lessons = fs.readdirSync("./lessons").map(lesson => {
        const config = require(path.resolve(
            "./lessons",
            lesson,
            "lesson.config.json"
        ))

        return config
    })

    res.send(JSON.stringify(lessons))
})

const upload = multer({ dest: "./temp_upload" })
app.post("/add", upload.single("file"), async (req, res) => {
    console.log(req.file)

    await extract(req.file.path, {
        dir: path.resolve(req.file.destination, "temp_dir"),
    })
    fs.unlinkSync(req.file.path)

    // fs.renameSync(
    //     path.join(req.file.destination, "temp_dir"),
    //     path.join(req.file.destination, req.file.filename)
    // )
    const configPath = path.resolve(
        req.file.destination,
        "temp_dir",
        "lesson.config.json"
    )
    const config = require(configPath)
    const name = config.name

    if (
        /^(con|prn|aux|nul|com[0-9]|lpt[0-9])$|([<>:"\/\\|?*])|(\.|\s)$/gi.test(
            name
        )
    ) {
        res.status(400).send("Invalid config name")
        return
    }
    if (!fs.existsSync("./lessons")) {
        fs.mkdirSync("./lessons")
    }
    if (fs.readdirSync("./lessons").includes(name)) {
        res.status(400).send("Lesson with such name already exists")
        return
    }

    fs.writeFileSync(configPath, JSON.stringify(config))

    fs.renameSync(
        path.join(req.file.destination, "./temp_dir"),
        path.join("./lessons", name)
    )

    res.status(200).send(config)
})

const PORT = 5000
app.listen(PORT, function () {
    console.log(`Example app listening on port ${PORT}!`)
})
